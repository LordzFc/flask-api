# Flask API for oldsaw
Simple backend to mock data. Written in only hour.

### Instalation

0. Download repo: 
  > git clone git@bitbucket.org:LordzFc/flask-api.git
1. Install pip https://pip.pypa.io/en/latest/installing.html
2. Install virtualenv:
  > pip install virtualenv
3. Create virtualenv:
  > virtualenv flask-api
4. Go to folder
  > cd flask-api
5. Activate isolated python enviroment:
  > source bin/activate (or source bin/activate.fish if you are using fishshell)
6. Install python packages:
  > pip install -r requirements.txt
7. Run application:
  > python app.py